# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Calculates the sum of the sample values.
"""


from sample_calculator.calculations import base

SUM_CALCULATION = "SUM"


class SumCalculation(base.Calculation):
    """ Calculates the sum of all sample values. """

    name = SUM_CALCULATION
    dependent_calculations = list()

    def _calculate(self, sample_values, dependent_values):
        return sum(sample_values)
