<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]()

## [0.2.0]()

### Added

- Added initial design addressing extensibility requirements including documentation and tests
- Provided implementation of the sum calculation
- Provided implementation of reading sample values from the command line
- Provided Makefile to automate tests, checks, and packaging
- Attached licensing (MIT, CC0-1.0) and copyright information
- Set up the issue tracker for team work
- Set up a build pipeline checking all changes before the get into master 

### Removed

- Removed the prototype implementation

## [0.1.0]() - 2020-06-02

> WARNING: Initial prototype which is not ready for productive use!

### Added

- Prototype calculates average, variance, and deviation.
- Sample values can be provided manually or by a file in CSV format.
